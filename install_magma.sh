#/bin/bash

cd ~/install_torch_on_hpc
source load_modules.sh
source load_depends.sh
echo "PATH=${PATH}"

if [[ ! -f magma-2.2.0.tar.gz ]]; then 
    wget "http://icl.cs.utk.edu/projectsfiles/magma/downloads/magma-2.2.0.tar.gz"
fi

mkdir -p magma-openblas
tar -xf magma-2.2.0.tar.gz -C magma-openblas
cp make.inc.magma-openblas magma-openblas/magma-2.2.0/make.inc
cp Makefile.magma  magma-openblas/magma-2.2.0/Makefile

cd magma-openblas/magma-2.2.0


make USE_FORTRAN=OFF -j4
make install prefix=/home/jxc761/local/magma

