#/bin/bash

cd ~/install_torch_on_hpc
source load_modules.sh

echo "PATH=${PATH}"
echo "CPATH=${CPATH}"
echo "C_INCLUDE_PAHT=${C_INCLUDE_PAHT}"
echo "CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}"
echo "LIBRARY_PATH=${LD_LIBRARY_PATH}"
echo "LD_LIBRARY_PATH=${LD_LIBRARY_PATH}"


if [[ ! -f v0.2.19.tar.gz ]]; then 
    wget  https://github.com/xianyi/OpenBLAS/archive/v0.2.19.tar.gz
fi

if [[ -d openblas ]]; then
    rm -rf openblas
fi    

mkdir openblas
tar -xf v0.2.19.tar.gz -C openblas

cd openblas/OpenBLAS-0.2.19
make NO_AFFINITY=1 USE_OPENMP=1
make PREFIX=/home/jxc761/local/openblas install
