#!/bin/bash

cd ~/install_torch_on_hpc
cp torch-patch/test.sh ~/torch
cp torch-patch/torch-activate ~/torch/install/bin
cp load_modules.sh ~/torch/install/bin
cp load_depends.sh ~/torch/install/bin

echo "

alias loadth=\". ${HOME}/torch/install/bin/torch-activate\"

" >> ~/.bashrc
