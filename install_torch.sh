#!/bin/bash

cd ~

source ~/install_torch_on_hpc/load_modules.sh
source ~/install_torch_on_hpc/load_depends.sh
echo "${PATH}"


rm -rf torch
git clone https://github.com/torch/distro.git ~/torch --recursive
cd ~/torch
./install.sh

