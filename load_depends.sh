#!/bin/bash
#
# Usage:
# 	source load_deps.sh
#
# jxc761@case.edu
# 04/06/2017
# 


# setting environment variables
PATH=${PATH:-}
CPATH=${CPATH:-}
LIBRARY_PATH=${LIBRARY_PATH:-}
LD_LIBRARY_PATH=${LD_LIBRARY_PATH:-}
DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH:-}
CMAKE_LIBRARY_PATH=${CMAKE_LIBRARY_PATH:-}
CMAKE_INCLUDE_PATH=${CMAKE_INCLUDE_PATH:-}

# if $2 have been included in $1 or $2 is an emtpy string, don't change
# else append $2 to $1
function nplab_addpath() {
	if [[ -z $2 ]] || [[  ":$1:" == *:$2:* ]]; then
	    echo "$1"
	else
	    echo "$2${1:+:${1}}"
	fi
}

function nplab_add() {
	local bin_dir=$1/${2:-bin}
	local inc_dir=$1/${3:-include}
	local lib_dir=$1/${4:-lib}

	PATH=$(nplab_addpath ${PATH} ${bin_dir})

	CPATH=$(nplab_addpath ${CPATH} ${inc_dir})
	C_INCLUDE_PATH=$(nplab_addpath ${C_INCLUDE_PATH} ${inc_dir})
	CPLUS_INCLUDE_PATH=$(nplab_addpath ${CPLUS_INCLUDE_PATH} ${inc_dir})
	CMAKE_INCLUDE_PATH=$(nplab_addpath ${CMAKE_INCLUDE_PATH} ${inc_dir})

	LIBRARY_PATH=$(nplab_addpath ${LIBRARY_PATH} ${lib_dir})
	DYLD_LIBRARY_PATH=$(nplab_addpath ${DYLD_LIBRARY_PATH} ${lib_dir})
	LD_LIBRARY_PATH=$(nplab_addpath ${LD_LIBRARY_PATH} ${lib_dir})
	CMAKE_LIBRARY_PATH=$(nplab_addpath ${CMAKE_LIBRARY_PATH} ${lib_dir})

}


nplab_add /home/jxc761/local/magma
nplab_add /home/jxc761/local/openblas
# nplab_add /home/jxc761/local/cudnn-v4
# nplab_add /usr/local/cuda-7.0 bin include lib64

export PATH
export CPATH
export LIBRARY_PATH
export LD_LIBRARY_PATH
export DYLD_LIBRARY_PATH
export CMAKE_LIBRARY_PATH
export CMAKE_INCLUDE_PATH


